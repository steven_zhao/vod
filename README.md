## ODV Project Overview

For reviewer:

Sorry for that, only got around 5 hours to finish this project, for some functions there might be a better solution.

Welcome to discuss!

## Main Technical Stacks:

Frontend: `react` + `react-router`

UI: `antd` + `sass`

Backend: `firebase`

Bundle: `webpack`

This main project structure was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Code to Setup

First

```javascript
npm install
```

Then

```javascript
npm start
```

Explaination for the technical stack choice:

As this project does not require complex api call and state management, so it's unnecessary to use `Redux` as the central state management tool, the native react state is very enough.

`Antd` is a good libraries to build UI, as there is no gape between antd and react, also it's quite simple to customize the UI widget inside antd.

In the backend, as we only need to store user watching history, besides, we do not need to handle authentication, so I take the `firebase real time database` service as our tool to build a cross platform "session like" backend. As if there is no authentication token, the session-cookie can only make persistent cross tab, but with firebase, we can make a cross platform, location, environment persistent system without user authentication.

Firebase real time database screenshot:

![alt text](screenshot/Capture.PNG)

In terms of the build bundle, all the config is hidden inside the project, if want to check the detail, just run:

```javascript
npm run eject
```

However, this process is irreversible, be careful.

## What's more

`Lazyload` is enble with carousel, `cover image` has error fallback and loading placeholder, `video player` is based on HTML5 video, and can auto close when finish playing, `navigation` can be made with both mouse and keyboard, `four breakpoints` to support responsiv, `tooltip` to check movie information, `modal` to display video and history information, auto compile script to realize `sass -> css`. `jest test` script is ready, just need to add snapshop or component unit test.

## To be improved

Add authentication to realize history persistent by user.
Implement a cloud server to realize `serverless concept`
After click from history modal, could play video directly.
