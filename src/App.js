import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import BasicLayout from "./containers/BasicLayout";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <BasicLayout className="app" />
      </BrowserRouter>
    );
  }
}

export default App;
