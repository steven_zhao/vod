import React from "react";
import Slider from "react-slick";
import PropTypes from "prop-types";
import VideoCard from "../VideoCard";

class Carousel extends React.PureComponent {
  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 5,
      arrows: false,
      lazyLoad: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 780,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <Slider {...settings}>
        {this.props.dataList.map((value, index) => {
          return <VideoCard data={value} key={index} />;
        })}
      </Slider>
    );
  }
}

Carousel.propTypes = {
  dataList: PropTypes.array.isRequired
};

export default Carousel;
