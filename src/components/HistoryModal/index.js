import React from "react";
import { Modal, List } from "antd";
import "./history-modal.css";
class HistoryModal extends React.Component {
  parseHistoryString = obj => {
    return "Video Id: " + obj["vid"] + " - Title: " + obj["title"];
  };

  render() {
    const data = Object.keys(this.props.historyList).reduce((acc, value) => {
      acc.push(this.parseHistoryString(this.props.historyList[value]));
      return acc;
    }, []);
    return (
      <Modal
        title="Watching History"
        visible={this.props.visible}
        onOk={this.props.close}
        onCancel={this.props.close}
      >
        <List
          className="list-item"
          size="large"
          bordered
          dataSource={data}
          renderItem={item => <List.Item>{item}</List.Item>}
        />
      </Modal>
    );
  }
}

export default HistoryModal;
