import React from "react";
import PropTypes from "prop-types";
import { Card, Tooltip, Icon, Button } from "antd";
import Modal from "../VideoModal";
import { updateHistory, getHistory } from "../../services/firebase";
import * as R from "ramda";
import "./video-card.css";

const { Meta } = Card;
class VideoCard extends React.Component {
  constructor(props) {
    super(props);
    this.imageUrl = props.data.images[0].url;
    this.state = {
      showModal: false,
      imageSrc: this.imageUrl
    };
    this.title = props.data.title;
    this.description = props.data.description;
    this.videoSrc = props.data.contents[0].url;

    if (this.description.length >= 60) {
      this.description = this.description.substring(0, 60) + " ...";
    }
  }

  showVideoModal = () => {
    let history = {};
    this.setState({
      showModal: true
    });
    getHistory().then(data => {
      history = data;
      let videoObj = {};
      videoObj[`${this.props.data.id}`] = {
        vid: this.props.data.id,
        title: this.props.data.title
      };
      updateHistory(R.merge(history, videoObj));
    });
  };
  closeVideoModal = () => {
    this.setState({
      showModal: false
    });
  };

  titleWithTooltip = () => {
    return (
      <span>
        <Tooltip title={this.props.data.description}>
          <span>
            <Icon type="info-circle" />
          </span>
        </Tooltip>{" "}
        {this.title}
      </span>
    );
  };

  error = () => {
    this.setState({
      imageSrc: "https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
    });
  };

  render() {
    return (
      <React.Fragment>
        <Card
          className="card"
          hoverable
          cover={
            <img src={this.state.imageSrc} alt="opps" onError={this.error} />
          }
        >
          <Meta
            title={this.titleWithTooltip()}
            description={this.description}
          />
        </Card>
        <Button onClick={this.showVideoModal} className="play-button">
          Play
        </Button>
        <Modal
          videoSrc={this.videoSrc}
          videoTitle={this.title}
          visible={this.state.showModal}
          close={this.closeVideoModal}
        />
      </React.Fragment>
    );
  }
}

VideoCard.propTypes = {
  data: PropTypes.object.isRequired
};

export default VideoCard;
