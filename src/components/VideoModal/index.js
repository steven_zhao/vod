import { Modal } from "antd";
import React from "react";
import "./video-modal.css";

class VideoModal extends React.Component {
  handleOk = e => {
    this.stopVideo();
    this.props.close();
  };

  handleCancel = e => {
    this.stopVideo();
    this.props.close();
  };

  stopVideo = () => {
    // security remove video instance
    const videoElement = document.getElementById("video-instance");
    videoElement.pause();
    videoElement.removeAttribute("src"); // empty source
    videoElement.load();
  };

  render() {
    return (
      <React.Fragment>
        <Modal
          title={this.props.videoTitle}
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          okText="Done"
          cancelText="Cancel"
        >
          <video
            width="460"
            height="270"
            controls
            className="video-player"
            id="video-instance"
            onEnded={this.handleOk}
          >
            <source src={this.props.videoSrc} type="video/mp4" />
          </video>
        </Modal>
      </React.Fragment>
    );
  }
}

export default VideoModal;
