import React from "react";
import { Route, Link, Switch, withRouter } from "react-router-dom";
import { Layout, Menu } from "antd";
import { getHistory } from "../../services/firebase";
import "./basic-layout.css";
import Home from "../HomeLayout";
import Notfound from "../../components/Notfound";
import HistoryModal from "./../../components/HistoryModal/index";
const { Header, Footer, Content } = Layout;

class BasicLayout extends React.Component {
  state = {
    historyModal: false,
    historyList: {}
  };

  constructor(props) {
    super(props);
    this.location = this.props.location.pathname;
  }

  showHistoryModal = () => {
    this.setState({
      showHistoryModal: true
    });
    getHistory().then(data => {
      this.setState({ historyList: data });
    });
  };

  closeHistoryModal = () => {
    this.setState({
      showHistoryModal: false
    });
  };

  render() {
    return (
      <Layout className="layout root-layout">
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[this.location]}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="/">
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item>
              <a onClick={this.showHistoryModal}>History</a>
            </Menu.Item>
          </Menu>
        </Header>
        <Content className="content-container">
          <div className="content">
            <Switch>
              <Route path="/" component={Home} exact />
              <Route component={Notfound} />
            </Switch>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          ©2018 Created by Steven Zhao
        </Footer>
        <HistoryModal
          visible={this.state.showHistoryModal}
          close={this.closeHistoryModal}
          historyList={this.state.historyList}
        />
      </Layout>
    );
  }
}

export default withRouter(BasicLayout);
