import React, { Component } from "react";
import { fetchMovieList } from "../../services/fetchMovieList";
import Carousel from "../../components/Carousel";
import "./home-layout.css";
class HomeLayout extends Component {
  state = {
    totalCount: 0,
    movieList: []
  };

  componentDidMount() {
    // call side effects to get movie list
    fetchMovieList().then(res => {
      this.setState({
        movieList: res.entries,
        totalCount: res.totalCount
      });
    });
  }
  render() {
    return (
      <div>
        <Carousel dataList={this.state.movieList} />
      </div>
    );
  }
}

export default HomeLayout;
