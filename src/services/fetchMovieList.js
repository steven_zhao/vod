import Promise from "bluebird";
import config from "../config/app.config";
import axios from "axios";
const MOVIE_LIST_ENDPOINT = config.MOVIE_LIST_ENDPOINT;

export const fetchMovieList = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(MOVIE_LIST_ENDPOINT)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else {
          throw new Error(`Server Error: ${res}`);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};
