import firebase from "firebase";
import Promise from "bluebird";

// initialize the firebase
const config = {
  apiKey: "AIzaSyDgH29xeihBLHJkYY0VJlnvgYkLiC-Fg1M",
  authDomain: "vod-db.firebaseapp.com",
  databaseURL: "https://vod-db.firebaseio.com",
  projectId: "vod-db",
  storageBucket: "vod-db.appspot.com",
  messagingSenderId: "382168577418"
};

const app = firebase.initializeApp(config);
const db = app.database();

// two customize services to update/get history
export const updateHistory = (history = {}) => {
  db.ref("history").set(history);
};

export const getHistory = () => {
  return new Promise((resolve, reject) => {
    const history = db.ref("history");
    history.on("value", function(snapshot) {
      resolve(snapshot.val());
    });
  });
};
